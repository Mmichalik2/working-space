from abc import ABC, abstractmethod
from functools import reduce

class Wezel(ABC):
    def __init__(self,liczby):
        self.liczba1 = liczby[0]
        self.liczba2 = liczby[1]

    @abstractmethod
    def nazwa(self):
        pass

    @abstractmethod
    def oblicz_wartosc(self):
        pass

    def wypisz_sie(self):
        print ("Wartosc wezla {} to: {}".format(self.nazwa(),self.oblicz_wartosc()))

class Liczba(Wezel):
    def __init__(self,liczba):
        self.liczba=liczba

    def oblicz_wartosc(self):
        return self.liczba

    @property
    def liczba(self):
        return self.__liczba

    @liczba.setter
    def liczba(self,liczba):
        self.__liczba=liczba

    def nazwa(self):
        return "Liczba"

    def __str__(self):
        return str(self.liczba)

class Dodawanie(Wezel):
    def __init__(self,*liczby):
        super().__init__(liczby)

    def oblicz_wartosc(self):
        return self.liczba1.liczba+self.liczba2.liczba

    def nazwa(self):
        return "Dodawanie dla liczb {} i {}".format(self.liczba1,self.liczba2)

class Odejmowanie(Wezel):
    def __init__(self,*liczby):
        super().__init__(liczby)

    def oblicz_wartosc(self):
        return self.liczba1.liczba+self.liczba2.liczba

    def nazwa(self):
        return "Odejmowanie"


class Mnozenie(Wezel):
    def __init__(self, *liczby):
        super().__init__(liczby)

    def oblicz_wartosc(self):
        return reduce((lambda x,y: x*y), self.liczby)

    def nazwa(self):
        return "Mnozenie"

class Dzielenie(Wezel):
    def __init__(self, *liczby):
        if liczby[1].liczba == 0 :raise ZeroDivisionError("Nie wolno dzielic przez zero!!")
        super().__init__(liczby)

    def oblicz_wartosc(self):
        return reduce((lambda x,y: x//y),self.liczby)

    def nazwa(self):
        return "Dzielenie"

class Silnia(Mnozenie):
    def __init__(self,liczby):
        if liczby.liczba < 0: raise ValueError("nie ma obsługi dla silni mniejszej od zera")
        self.liczby=range(1,liczby+1)

    def oblicz_wartosc(self):
        return super().oblicz_wartosc()

    def nazwa(self):
        return "Silnia"

    def wypisz_sie(self):
        print("Wartosc wezla {} dla liczb {} to: {}".format(self.nazwa(), self.liczby[-1], self.oblicz_wartosc()))


try:
    dwa=Liczba(2)
    dwadziescia=Liczba(20)
    zero=Liczba(0)
    minuspiec=Liczba(-5)
    Dodawanie(dwa,dwadziescia).wypisz_sie()
    #Dzielenie(dwadziescia,zero).wypisz_sie()
    Silnia(minuspiec).wypisz_sie()
except Exception as e:
    print (e,str(type(e)))
# print(Mnozenie(2,3,4,5,6,7,8,9).oblicz_wartosc())
# print (Silnia(9).oblicz_wartosc())
#
# Dzielenie(8,4).wypisz_sie()
# Silnia(9).wypisz_sie()
# jeden=Liczba(1)
# Dodawanie(jeden,dwa)=3
#
# DO NAPRAWY