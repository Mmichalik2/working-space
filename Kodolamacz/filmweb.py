from bs4 import BeautifulSoup
import requests
import re
import unicodedata

# filmweb_site=requests.get("https://www.filmweb.pl/film/Narodziny+gwiazdy-2018-542576").text
# fw_soup=BeautifulSoup(filmweb_site,'html.parser')
#
# director_a=fw_soup.find('a', itemprop="director")
# print (director_a["title"])
#
# premiere_href=fw_soup.find("a", href=re.compile(".*dates")).find("span").text
# print (premiere_href)
#
# boxoffice_div=fw_soup.find("div", text="boxoffice").next_sibling.contents
# print("\n".join([boxoffice_text.text for boxoffice_text in boxoffice_div]))
#
# rating=round(float(fw_soup.find("div", class_=re.compile("filmRating.*"))["datarating-rate"]),2)
# print (rating)

class Film:
    def __init__(self,title):
        self.link=self._find_link(title)
        self.soup_site=self._soup_site()
        self.director=self._find_director()
        self.boxoffice=self._find_boxoffice()
        self.date=self._find_date()
        self.rating = Rating(self.soup_site)
        self.real_title=self._find_title()

    def __str__(self):
        return ("znaleziony film to: {}\n"
                "Film mial premiere: {}\n"
                "Rezyserem jest: {}\n"
                "Boxoffice wyniosl: {}\n"
                "Aktualny rating to: \n{}".format(self.real_title,self.date,self.director,", ".join(self.boxoffice), self.rating))


    def _find_link(self,title):
        ascii_title=unicodedata.normalize('NFKD', title).encode('ascii', 'ignore')
        search_site=requests.get("https://www.filmweb.pl/search", params={"q":ascii_title}).text
        search_site_soap=BeautifulSoup(search_site,'html.parser')
        filmh2=search_site_soap.find("h2", text=re.compile(title,re.IGNORECASE))
        return filmh2.parent["href"]

    def _soup_site(self):
        site_text=requests.get("https://www.filmweb.pl"+self.link).text
        return BeautifulSoup(site_text, 'html.parser')

    def _find_title(self):
        return self.soup_site.find("h1", itemprop="name").next_element.text

    def _find_director(self):
        return self.soup_site.find("h3", text="reżyseria").next_sibling.next_element["title"]

    def _find_boxoffice(self):
        try:
            div_list=self.soup_site.find("div", text="boxoffice").next_sibling.find_all("div")
        except:
            print ("Brak danych na temat boxoffice")
            return "NA"
        return [boxoffice.text for boxoffice in div_list]

    def _find_date(self):
        return self.soup_site.find("h3", text="premiera").next_sibling.next_element.text

    def update_rating(self):
        self.rating.update_rating()

class Rating:
    def __init__(self,film_site):
        self.film_site=film_site
        self.rate=self.get_rate(film_site)
        self.count=self.get_count(film_site)

    def __str__(self):
        return "rate: "+str(self.rate)+"\ncount: "+str(self.count)

    @classmethod
    def get_value(cls,film_site,param):
        return float(film_site.find("span", class_="filmRating__"+param).contents[0].replace(",",".").replace(" ", ""))

    @classmethod
    def get_rate(cls,film_site):
        return Rating.get_value(film_site,"rateValue")

    @classmethod
    def get_count(cls,film_site):
        return Rating.get_value(film_site,"count")

    def update_rating(self):
        print("Aktualizacja ratingu:")

        old_rate = self.rate
        old_count = self.count
        self.rate = self.get_rate(self.film_site)
        self.count = self.get_count(self.film_site)
        if self.rate==old_rate and self.count==self.get_count(self.film_site):
            print ("bez zmian")
        else:
            print ("Wartosc: {}\nIlosc: {}".format(self.rate-old_rate,self.count-old_count))

print (Film("smoleńsk"))





#print (Rating.get_rate(Film("narodziny gwiazdy").soup_site))



