def potega_wydajnie(podstawa,wykladnik):
    if wykladnik==1: return podstawa
    if wykladnik==0: return 1
    if wykladnik%2: return potega_wydajnie(podstawa,(wykladnik-1)//2)*potega_wydajnie(podstawa,(wykladnik-1)//2)*podstawa
    return potega_wydajnie(podstawa,wykladnik/2)*potega_wydajnie(podstawa,wykladnik/2)



def czyPalindrom(slowo):
    return slowo == slowo[::-1]


print(czyPalindrom("kajak"))

def czyAnagram(pierwsze_slowo,drugie_slowo):
    if set(pierwsze_slowo)!=set(pierwsze_slowo+drugie_slowo): return False
    licznik = lambda slowo: {litera:slowo.count(litera) for litera in slowo}
    if any(licznik(pierwsze_slowo)[litera]<licznik(drugie_slowo)[litera] for litera in licznik(drugie_slowo)):return False
    return True

print (czyAnagram("aaadddghyyyws","dddaawaghy"))


def moda(lista):
    slownik={liczba:lista.count(liczba) for liczba in lista}
    maxim=max(slownik.values())
    for liczba,wystapienia in slownik.items():
        if wystapienia==maxim:return liczba


print (moda([2,3,4,5,33,5,6]))

print("2^10 = " + str(potega_wydajnie(2, 10)))
print("czyPalindrom(kajak) = " + str(czyPalindrom("kajak")))
print("czyPalindrom(kobyla) = " + str(czyPalindrom("kobyla")))
print("czyPalindrom2(kajak) = " + str(czyPalindrom("kajak")))
print("czyPalindrom2(kobyla) = " + str(czyPalindrom("kobyla")))
print("czyAnagram(kajak, jaakk) = " + str(czyAnagram("kajak", "jaakk")))
print("czyAnagram(kobyla, boczek) = " + str(czyAnagram("kobyla", "boczek")))
print("moda([1,6,4,7,2,8,6,7,6]) = " + str(moda([1, 6, 4, 7, 2, 8, 6, 7, 6])))