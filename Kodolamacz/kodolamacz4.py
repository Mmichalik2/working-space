class FunkcjaKwadratowa:
    def __init__(self, a, b, c):
        self.a = a
        if self.a == 0: raise RuntimeError("a nie moze byc zerem")
        self.b = b
        self.c = c
        self.obliczdelte()
        if self.delta < 0: raise RuntimeError("nie bedzie miejsc zerowych")
        self.obliczmiejscazerowe()
        print(self.miejscazerowe)

    def __str__(self):
        return '{}x^2+{}x+{}'.format(self.a, self.b, self.c)

    def obliczdelte(self):
        self.delta = self.b ** 2 - 4 * self.a * self.c

    def obliczmiejscazerowe(self):
        self.miejscazerowe = set(
            [(-self.b + self.delta ** 0.5) / (2 * self.a), (-self.b - self.delta ** 0.5) / (2 * self.a)])


class Zespolona:

    def __init__(self, re, im):
        self.re = re
        self.im = im

    def modul(self):
        return (self.re ** 2 + self.im ** 2) ** 0.5

    def __str__(self):
        return ("{re}+{im}i".format(re=self.re, im=self.im))

    @staticmethod
    def dodaj(zespolona1, zespolona2):
        return (Zespolona(zespolona1.re + zespolona2.re, zespolona1.im + zespolona2.im))

    @staticmethod
    def mnoz(zespolona1, zespolona2):
        return Zespolona(zespolona1.re * zespolona2.re - zespolona1.im *
                         zespolona2.im, zespolona1.re * zespolona2.im + zespolona2.re * zespolona1.im)


class Ulamek:
    def __init__(self, licznik, mianownik):
        if mianownik == 0:
            raise ZeroDivisionError ("pamietaj cholero, nie dziel przez zero!!!")
        self.licznik = licznik
        self.mianownik = mianownik

    def __add__(self, ulamek2):
        return Ulamek.dodaj(self, ulamek2)

    def __sub__(self, other):
        return Ulamek.odejmij(self, other)

    def __mul__(self, other):
        return Ulamek.mnoz(self, other)

    def __truediv__(self, other):
        return Ulamek.dziel(self, other)

    def __str__(self):
        return ("{licznik}/{mianownik}".format(licznik=self.licznik, mianownik=self.mianownik))

    @staticmethod
    def dzielniki(liczba):
        return [abs(x) for x in range(1, abs(liczba) + 1) if liczba % x == 0]

    @staticmethod
    def wspolnydzielnik(liczba1, liczba2):
        return max([abs(x) for x in Ulamek.dzielniki(liczba1) if x in Ulamek.dzielniki(liczba2)])

    def skroc(self):
        if not (self.licznik and self.mianownik):
            print("mianownik jest zerem, nie mozna skrocic")
        else:
            nwd = self.wspolnydzielnik(self.mianownik, self.licznik)
            self.mianownik //= nwd
            self.licznik //= nwd

    @staticmethod
    def dodaj(ulamek1, ulamek2):
        ulamekdodany = Ulamek(ulamek1.licznik * ulamek2.mianownik + ulamek2.licznik * ulamek1.mianownik,
                              ulamek1.mianownik * ulamek2.mianownik)
        ulamekdodany.skroc()
        return ulamekdodany

    @staticmethod
    def odejmij(ulamek1, ulamek2):
        ulamekodjety = Ulamek.dodaj(ulamek1, Ulamek(-ulamek2.licznik, ulamek2.mianownik))
        return ulamekodjety

    @staticmethod
    def mnoz(ulamek1, ulamek2):
        ulamekpomnozony = Ulamek(ulamek1.licznik * ulamek2.licznik, ulamek1.mianownik * ulamek2.mianownik)
        ulamekpomnozony.skroc()
        return ulamekpomnozony

    @staticmethod
    def dziel(ulamek1, ulamek2):
        return ulamek1 * Ulamek(ulamek2.mianownik, ulamek2.licznik)


# print(Ulamek.wspolnydzielnik(-5,30))
#
#
# #
# x=Ulamek(851029,-9231711)
# x.skroc()
# print(x)

# Ulamek.dodaj(Ulamek(3,5),Ulamek(9,15))
try:
    print((Ulamek(3, 5) / Ulamek(10, 15)))

#
# print(Zespolona.dodaj(Zespolona(2, 5), Zespolona(6, 9)))
# print(Zespolona.mnoz(Zespolona(2, 5), Zespolona(6, 9)))
    print (Ulamek(5,0))
    print(FunkcjaKwadratowa(2, 4, 1))
except ZeroDivisionError as zero:
    print (zero)

finally:
    print ("koniec programu")