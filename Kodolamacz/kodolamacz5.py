#dziedziczenie, polimorfizm, hermetyzacja i abstrakcja
#Stwórz hierarchię klas reprezentujących figury geometryczne.
#Każda figura powinna umieć wypisać informacje o sobie, a także obliczyć swój obwód i pole.
# W grę niech wchodzą koła, prostokąty, kwadraty oraz trójkąty.

from abc import ABC, abstractmethod

class Figura(ABC):
    def __init__(self,kolor):
        self.kolor=kolor
        self.pole=self.oblicz_pole()
        self.obwod=self.oblicz_obwod()

    @abstractmethod
    def oblicz_obwod(self):
        pass

    @abstractmethod
    def oblicz_pole(self):
        pass

    @abstractmethod
    def podaj_nazwe(self):
        pass

    @property
    def kolor(self):
        return self.__kolor
    @kolor.setter
    def kolor(self,kolor):
        if kolor in ["zolty","zielony","niebieski"]:self.__kolor=kolor
        else: self.__kolor="nieznany"

    def przedstaw_sie(self):
        print("Jestem {} o kolorze {}. Moje pole to: {}, a obwod to {}".format(self.podaj_nazwe(), self.kolor,self.oblicz_pole(),self.oblicz_obwod()))

class Kolo(Figura):
    pi=22/7
    def __init__(self,promien,kolor):
        self.promien=promien
        super().__init__(kolor)

    def oblicz_obwod(self):
        return Kolo.pi*self.promien**2

    def oblicz_pole(self):
        return Kolo.pi*2*self.promien

    def podaj_nazwe(self):
        return "Kolo"

    def przedstaw_sie(self):
        super().przedstaw_sie()
        print("a promien to"+str(self.promien))

class Prostokat(Figura):
    def __init__(self,kolor,a,b):
        self.a=a
        self.b=b
        super().__init__(kolor)

    def oblicz_obwod(self):
        return self.a*2+self.b*2

    def oblicz_pole(self):
        return self.a*self.b

    def podaj_nazwe(self):
        return "Prostokat"

class Kwadrat(Prostokat):
    def __init__(self,kolor,a):
        super.__init__(kolor,a,a)

    def podaj_nazwe(self):
        return "Kwadrat"

class Trojkat(Figura):
    def __init__(self,kolor,a,b,c,h):
        self.a=a
        self.b=b
        self.c=c
        self.h=h
        super().__init__(kolor)

    @property
    def a(self):
        return self.__a

    def

    @a.setter
    def a(self,a):
        if not (self.c and self.b) or self.b+self.c>a: self.__a=a
        else: self.__a=self.b+self.c-1



    @property
    def b(self):
        return self.__b

    @property
    def c(self):
        return self.__c


    def oblicz_obwod(self):
        return self.a*2+self.b*2

    def oblicz_pole(self):
        return self.a*self.b

    def podaj_nazwe(self):
        return "trojkat"


Trojkat("czarny",2,3,4,5).przedstaw_sie()

Kolo(5,"zielony").przedstaw_sie()