class RomanNumerals:
    translate={'M':1000,'D':500,'C':100,'L':50,'X':10,'V':5,"I":1}

    @staticmethod
    def from_roman_order(number):
        to_subtract=[x for x in number[:-1] if RomanNumerals.translate[number[number.index(x)+1]]>RomanNumerals.translate[x]]
        return to_subtract

    @staticmethod
    def from_roman_sum(number):
        x=sum(RomanNumerals.translate[sign] for sign in number)
        return x

    @staticmethod
    def from_roman(number):
        return RomanNumerals.from_roman_sum(number)-2*RomanNumerals.from_roman_sum(RomanNumerals.from_roman_order(number))

    @staticmethod
    def to_roman(number):
        roman=''
        for key,value in RomanNumerals.translate.items():
            subtractor = [x for x in list(RomanNumerals.translate.keys())[2::2] if RomanNumerals.translate[x] < number]
            if number%value!=number:
                roman=roman+(key*int(number/value))
                number=number%value
            if subtractor and number>0 and (number+RomanNumerals.translate[subtractor[0]])%value!=number+RomanNumerals.translate[subtractor[0]]:
                roman=roman+(subtractor[0]+key)
                number=(number+RomanNumerals.translate[subtractor[0]])%value
        return roman



        #romansigns=[RomanNumerals.translate[]]

#print(RomanNumerals.from_roman('MMVIII'))

print(RomanNumerals.to_roman_compose(999))

