from future.moves import tkinter as tk


window = tk.Tk()

greeting = tk.Label(
    text="Hello",
    foreground="white",
    background="black",
    width=10,
    height=10)
greeting.pack()

button = tk.Button(
    text="Click me!",
    width=15,
    height=5,
    bg="blue",
    fg="yellow",
)

label = tk.Label(text="Name")
entry = tk.Entry()
label.pack()
entry.pack()
button.pack()
window.mainloop()

