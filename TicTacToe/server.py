import socket
from main import game

HOST = '127.0.0.1'  # Standard loopback interface address (localhost)
PORT = 2137       # Port to listen on (non-privileged ports are > 1023)


def turn(con,multi):
    if not multi.tabela_gry.sign:
        con.send("wait, it's X turn".encode())
        multi.tabela_gry.fill(int(input("Podaj wiersz: ")),int(input("Podaj kolumne: ")))
        con.send((multi.tabela_gry.__str__()).encode())
    else:
        print ("wait, it's Y turn")
        con.send("Podaj wiersz: ".encode())
        wiersz= con.recv(1024).decode()
        con.send("Podaj kolumne: ".encode())
        kolumna= con.recv(1024).decode()
        multi.tabela_gry.fill(int(wiersz), int(kolumna))
        con.send((multi.tabela_gry.__str__()).encode())




multi = game(int(input("Wprowadz rozmiar tablicy: ")))
with socket.socket(socket.AF_INET, socket.SOCK_STREAM) as s:
    s.bind(('', PORT))
    s.listen()
    conn, addr = s.accept()
    with conn:
        print('game started with: ', addr)
        while not multi.tabela_gry.checkwin():
            if multi.tabela_gry.checkdraw():
                conn.send(("It's a draw!").encode())
                break
            turn(conn,multi)
        if not multi.tabela_gry.checkdraw():
            conn.send((multi.tabela_gry.xo[not multi.tabela_gry.sign] + " WIN").encode())

        s.close()
        # data=True
        # while data:
        #     print (data.decode())
        #     data = conn.recv(1024)






