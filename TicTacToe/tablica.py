

class tablica:

    def __init__(self,size=3): #inicjalizacja tabeli
        self.tabela = [["+"] * size for _ in range (size)]
        self.xo={False:"X",True:"Y"}
        self.sign=False
        self.size=size

    def __str__(self): #printowanie tabeli
        toprint=""
        for x in self.tabela:
            toprint=toprint+"|"+"|".join(x)+"|\n"
        return toprint

    def fill(self,wiersz,kolumna):
        if wiersz<self.size and kolumna<self.size and self.tabela[wiersz][kolumna] == "+":
            self.tabela[wiersz][kolumna]=self.xo[self.sign].upper()
            print(self)
            self.sign=not self.sign
        else:
            print ((self.xo[self.sign].upper())+" wprowadz jeszcze raz\n")

    def checkline(self,line):
        for x in range(1,len(line)):
            if line[x]!=line[x-1] or line[x]=="+":
                return False
        print(self.xo[not self.sign] + " WIN")
        return True

    def checkwin(self):
        for line in self.tabela:
            if self.checkline(line):return True
        for columnnumber in range (len(self.tabela)):
            if self.checkline([column[columnnumber] for column in self.tabela]):return True
        if self.checkline([self.tabela[i][i] for i in range (len(self.tabela))]):return True
        if self.checkline([self.tabela[i][len(self.tabela)-1-i] for i in range (len(self.tabela))]):return True
        return False
    def checkdraw(self):
        if not "+" in sum(self.tabela,[]):
            print ("It's a draw!")
            return True
